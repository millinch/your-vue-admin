// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import store from './store'
import Element from 'element-ui'
import 'normalize.css/normalize.css'
import 'element-ui/lib/theme-chalk/index.css'
import '@/styles/index.scss' // global css
import i18n from './lang'
import './icons/iconfont.js'
import './icons/iconfont.css'
import SvgIcon from '@/components/SvgIcon'// svg组件

Vue.config.productionTip = false
Vue.use(Element, {
  size: 'medium',
  i18n: (key, val) => {
    return i18n.t(key, val) || ''
  }
})
Vue.component('svg-icon', SvgIcon)
/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  i18n,
  components: { App },
  template: '<App/>'
})
